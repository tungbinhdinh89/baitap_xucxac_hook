import './App.css';
import TaiXiu from './GameTaiXiu/TaiXiu';

function App() {
  return (
    <div className="App">
      <TaiXiu />
    </div>
  );
}

export default App;
