import React, { useCallback, useState } from 'react';
import bg_game from './assets/bgGame.png';
import './TaiXiu.css';
import XucXac from './XucXac';
import KetQua from './KetQua';
export const TAI = 'TÀI';
export const XIU = 'XỈU';
export default function TaiXiu() {
  const [xucXacArr, setXucXacArr] = useState([
    { img: `./imgXucXac/1.png`, value: 1 },
    { img: `./imgXucXac/1.png`, value: 1 },
    { img: `./imgXucXac/1.png`, value: 1 },
  ]);
  const [luotChoi, setLuotChoi] = useState(0);
  const [banThang, setBanThang] = useState(0);
  const [luaChon, setLuaChon] = useState(null);
  const [result, setResult] = useState(' ');

  // Đặt cược
  const handleDatCuoc = (value) => {
    setLuaChon(value);
  };

  // play game
  const handlePlayGame = () => {
    let tongDiem = 0;
    let newXucXac = xucXacArr.map(() => {
      let numberRandom = Math.floor(Math.random() * 6) + 1;
      tongDiem += numberRandom;
      return {
        img: `./imgXucXac/${numberRandom}.png`,
        value: numberRandom,
      };
    });
    tongDiem > 11 && luaChon == TAI && setBanThang(banThang + 1);

    tongDiem <= 11 && luaChon == XIU && setBanThang(banThang + 1);

    (tongDiem > 11 && luaChon == TAI) || (tongDiem <= 11 && luaChon == XIU)
      ? setResult('YOU WON')
      : setResult('YOU LOSE');
    setLuotChoi(luotChoi + 1);
    setXucXacArr(newXucXac);
  };
  return (
    <div className="game">
      <h1 className="display-4">Tài Xỉu Game</h1>
      <XucXac xucXacArr={xucXacArr} handleDatCuoc={handleDatCuoc} />
      <KetQua
        luaChon={luaChon}
        luotChoi={luotChoi}
        banThang={banThang}
        handlePlayGame={handlePlayGame}
        result={result}
      />
    </div>
  );
}
