import React, { useState } from 'react';
import { TAI, XIU } from './TaiXiu';

export default function KetQua({
  luaChon,
  luotChoi,
  banThang,
  handlePlayGame,
  result,
}) {
  return (
    <div>
      {(luaChon == TAI || luaChon == XIU) && (
        <button
          className="btn btn-success px-5 py-3 my-3 display-4"
          style={{ fontSize: 25 }}
          onClick={() => {
            handlePlayGame();
          }}>
          Play Game
        </button>
      )}
      <div>
        {luotChoi > 0 && (
          <h1 className="text-warning display-4 px-2">{result}</h1>
        )}
      </div>
      <h1 className="display-4 text-primary px-2">Bạn chọn: {luaChon}</h1>
      <h1 className="display-4 text-danger px-2">Số lượt chơi : {luotChoi}</h1>
      <h1 className="display-4 text-success px-2">Số lần thắng : {banThang}</h1>
    </div>
  );
}
