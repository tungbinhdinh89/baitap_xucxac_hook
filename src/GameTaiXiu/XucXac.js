import React from 'react';
import { TAI, XIU } from './TaiXiu';

export default function XucXac({ xucXacArr, handleDatCuoc }) {
  const renderXucXac = () => {
    return xucXacArr.map((xucXac, index) => {
      return (
        <img
          className="m-2 my-5"
          style={{ height: 100, width: 100 }}
          src={xucXac.img}
          alt="xucXac Image"
          key={index}
        />
      );
    });
  };

  return (
    <div className="p-5 d-flex justify-content-between container ">
      <button
        className="btn btn-success rounded btnGame"
        onClick={() => {
          handleDatCuoc(TAI);
        }}>
        Tài
      </button>
      <div>{renderXucXac()}</div>
      <button
        className="btn btn-primary rounded btnGame"
        onClick={() => {
          handleDatCuoc(XIU);
        }}>
        Xỉu
      </button>
    </div>
  );
}
